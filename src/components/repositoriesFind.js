import React from 'react';
import { Form, Button, Row, Container, Col } from 'react-bootstrap';
import RepositoriesList from './repositoriesList';
import CommitsList from './commitsList';


class RepositoriesFind extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      name: '',
      repositories: [],
      selectedRepo: "",
      repoCommits: []
  };  

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlerRepoSelect = this.handlerRepoSelect.bind(this);
  }

  reposList() {
    console.log(this.state)
    if (this.state.repositories.length > 0) {
      return < RepositoriesList repos={this.state.repositories} />
    }
    return <div>No repositories</div>
  }

  handleChange(event) {
    this.setState({name: event.target.value});
  }

  getCommits() {
    console.log(this.state.selectedRepo)
    const url = `https://api.github.com/repos/${this.state.name}/${this.state.selectedRepo}/commits`;
    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ repoCommits: data }))
  }

  handlerRepoSelect(data) {
    this.setState({selectedRepo: data}, () => {
      this.getCommits();
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const url = `https://api.github.com/users/${this.state.name}/repos`;
    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ repositories: data }))
  }

   render() {
      let listRepositories;
      let listCommits;
      if (this.state.repositories.length > 0) {
        listRepositories = < RepositoriesList params={this.state.repositories} onSelectRepo={this.handlerRepoSelect} />
      } else {
        listRepositories = <div>No repositories</div>
      }

      if (this.state.repoCommits.length > 0) {
        listCommits = < CommitsList params={this.state.repoCommits} />
      } else {
        listCommits = <div>No commits</div>
      }

      return (
        <Container>
          <Row className="justify-content-md-center">
            <Col md={6}>
              <Form onSubmit={this.handleSubmit} float="center">
                <Form.Group>
                  <Form.Label>user name</Form.Label>
                  <Form.Control 
                    placeholder="Enter name" 
                    value={this.state.name} 
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
              </Col>
          </Row>
          <Row>
            <Col>{ listRepositories }</Col>
            <Col>{ listCommits }</Col>
          </Row>
        </Container>
      );
   }
}
export default RepositoriesFind;