import React from 'react';
import { ListGroup, Row } from 'react-bootstrap';

class CommitsList extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      commits: props.params
    };
   }

  componentWillReceiveProps(props) {
    this.setState({ commits: props.params });  
  }
   render() {
      const items = this.state.commits.map((item, key) => {
        return <ListGroup.Item
        key={key} 
        >
          <div className="text-left">Author: {item.commit.author.name}</div>
          <div className="text-left">Message: {item.commit.message}</div>
          <div className="text-left">Date: {item.commit.committer.date}</div>
        </ListGroup.Item>
      })

      return (
        <ListGroup>
          {items}
        </ListGroup>
      );
   }
}
export default CommitsList;