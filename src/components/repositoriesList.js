import React from 'react';
import { ListGroup } from 'react-bootstrap';

class RepositoriesList extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      repositories: props.params
    };
    this.handlerRepoSelect = this.handlerRepoSelect.bind(this);
   }

   handlerRepoSelect(e, itemName) {
    this.props.onSelectRepo(itemName);
  }

   render() {
      const items = this.state.repositories.map((item, key) => {
        return <ListGroup.Item
        key={key} 
        onClick={(e) => this.handlerRepoSelect(e, item.name)}
        value={item.name}
        >{item.name}</ListGroup.Item>
      })

      return (
        <ListGroup>
          {items}
        </ListGroup>
      );
   }
}
export default RepositoriesList;